import Reveal from "../../dist/reveal.esm.js"
import RevealMarkdown from "../../plugin/markdown/markdown.esm.js"
import RevealHighlight from "../../plugin/highlight/highlight.esm.js"
import RevealMenu from "../../plugin/reveal.js-plugins/menu/menu.esm.js"
import RevealZoom from "../../plugin/zoom/zoom.esm.js"
import RevealNotes from "../../plugin/notes/notes.esm.js"
import RevealSearch from "../../plugin/search/search.esm.js"

Reveal.initialize({
    // Push each slide change to the browser history
    history: true,
    slideNumber: 'c/t',

    plugins: [ RevealMarkdown, RevealHighlight, RevealChalkboard, RevealMenu, RevealNotes, RevealSearch ],

    chalkboard: {
        chalkWidth: 4,
        chalkEffect: 0.0,
        toggleChalkboardButton: { left: "70px", bottom: "30px", top: "auto", right: "auto" },
        toggleNotesButton: { left: "110px", bottom: "30px", top: "auto", right: "auto" }
    },

    menu: {
        custom: [
            { title: 'Sections', icon: '<i class="fas fa-file-powerpoint"></i>', src: '../menu.html' }
        ]
    }
});
